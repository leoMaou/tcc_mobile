# tcc_mobile

#### Informações

**atz** : pegar modelo do scanner ( testei )

**atrv**: pegar voltagem do veiculo ( testei )

**atsp0**: com engine ligada, pegar melhor protocolo para o veiculo e scanner ( testei )

**atdp**: descreve o atual protocolo (Não testei)

**attp h**: seta protocolo, substituir variavel h (Não testei)


##### Os comandos de modes e serviços só podem ser executos após setar o protocolo

1. mode 01 
   * informação atual no scanner, ele possui varios serviços

1. serviço 00 
   * traz os serviços suportados (confuso no teste que fiz)

1. mode 03 
   * traz os codigos de problema (ainda não testei)

1. mode 04 
   * limpa os códigos de erro (ainda não testei)


### Datasheet de comandos

[link](https://cdn.sparkfun.com/assets/c/8/e/3/4/521fade6757b7fd2768b4574.pdf)

### Standarts PIDs e Serviços

[link](https://en.wikipedia.org/wiki/OBD-II_PIDs)


#### Generate models com slidy

```bash
slidy generate model models/falha_detectada
```
