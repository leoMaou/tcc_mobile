import 'package:tcc_mobile/app/models/enum/falha_detectada_enum.dart';

class FalhaDetectadaModel {
  String descricao = "";
  String codigo;
//  DateTime data_detectado;

  FalhaDetectadaModel(String codigo) {

    FalhaEnum falhaEnum = getFalhaFromString(codigo);
    this.codigo = codigo;

    if(falhaEnum != null) {
      this.descricao = FalhasHelper.getValue(falhaEnum);;
    }else {
      this.descricao = "Codigo desconhecido";
    }

  }

  FalhaEnum getFalhaFromString(String falha) {
    falha = 'FalhaEnum.$falha';
    return FalhaEnum.values.firstWhere((f)=> f.toString() == falha, orElse: () => null);
  }
}
