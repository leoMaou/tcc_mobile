import 'dart:ffi';

class ConfiguracaoModel {

  // database table and column names
  final String tableConfig = 'configuracao';
  final String columnId = '_id';
  final String columnAno = 'ano';
  final String columnFabricante = 'fabricante';
  final String columnTipoVeiculo = 'tipo_veiculo';
  final String columnCombustivel = 'combustivel';
  final String columnTamanhoMotor = 'tamanho_motor';
  final String columnEficienciaMotor = 'eficiencia_motor';
  final String columnConsumoCombustivel = 'consumo_combustivel';
  final String columnCapacidadeTanque = 'capacidade_tanque';
  final String columnCustoUnidade = 'custo_unidade';
  final String columnFatorEscala = 'fator_escala';


  int id;
  int ano;
  String fabricante;
  String tipo_veiculo;
  String combustivel;
  String tamanho_motor;
  String eficiencia_motor;
  double consumo_combustivel;
  double capacidade_tanque;
  double custo_unidade;
  double fator_escala;


  ConfiguracaoModel({this.ano, this.fabricante, this.tipo_veiculo, this.combustivel,
    this.tamanho_motor, this.eficiencia_motor, this.consumo_combustivel, this.capacidade_tanque,
    this.custo_unidade, this.fator_escala});


  // convenience constructor to create a Word object
  ConfiguracaoModel.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    ano = map[columnAno];
    fabricante = map[columnFabricante];
    tipo_veiculo = map[columnTipoVeiculo];
    combustivel = map[columnCombustivel];
    tamanho_motor = map[columnTamanhoMotor];
    eficiencia_motor = map[columnEficienciaMotor];
    consumo_combustivel = map[columnConsumoCombustivel];
    capacidade_tanque = map[columnCapacidadeTanque];
    custo_unidade = map[columnCustoUnidade];
    fator_escala = map[columnFatorEscala];
  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnAno: ano,
      columnFabricante: fabricante,
      columnTipoVeiculo: tipo_veiculo,
      columnCombustivel: combustivel,
      columnTamanhoMotor: tamanho_motor,
      columnEficienciaMotor: eficiencia_motor,
      columnConsumoCombustivel: consumo_combustivel,
      columnCapacidadeTanque: capacidade_tanque,
      columnCustoUnidade: custo_unidade,
      columnFatorEscala: fator_escala
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

}
