enum ObdComands {
  TEST_ARDUINO,
  PEGAR_MODELO_SCANNER,
  PEGAR_VOLTAGEM_VEICULO,
  SETAR_MELHOR_PROTOCOLO_PARA_LEITURA,
  SETAR_PROTOCOLO_AUTOMATICO,
  PEGAR_PROTOCOLO_SETADO,
  RESETAR,
  SETAR_PADRAO,
  VERIFICAR_SE_EXISTEM_FALHAS,
  LIMPAR_CODIGOS_DE_PROBLEMAS_E_VALORES_SALVOS,
  BUCAS_FALHAS_DO_VEICULO,
}

class ObdComandsHelper {

  static String getValue(ObdComands mode){
    switch(mode){
      case ObdComands.TEST_ARDUINO:
        return "L";
      case ObdComands.PEGAR_VOLTAGEM_VEICULO:
        return "atrv";
      case ObdComands.SETAR_MELHOR_PROTOCOLO_PARA_LEITURA:
        return "atsp0";
      case ObdComands.PEGAR_PROTOCOLO_SETADO:
        return "atdp";
      case ObdComands.RESETAR:
        return "atz";
      case ObdComands.SETAR_PADRAO:
        return "atd";
      case ObdComands.VERIFICAR_SE_EXISTEM_FALHAS:
        return "0101";
      case ObdComands.BUCAS_FALHAS_DO_VEICULO:
        return "03";
      case ObdComands.LIMPAR_CODIGOS_DE_PROBLEMAS_E_VALORES_SALVOS:
        return "04";
      default:
        return "";
    }
  }

}