enum FalhaEnum {
  P0351,
  P0352,
  P0353,
  P0354,
  P0355,
  P0356,
  P0357,
  P0358,
  P0359,
  P0360,
  P0361,
  P0362
}

class FalhasHelper {


  static String getValue(FalhaEnum falha_code){
    switch(falha_code){
      case FalhaEnum.P0351:
        return "Circuito Primário / Secundário numero 1 (A) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0352:
        return "Circuito Primário / Secundário numero 2 (B) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0353:
        return "Circuito Primário / Secundário numero 3 (C) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0354:
        return "Circuito Primário / Secundário numero 4 (D) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0355:
        return "Circuito Primário / Secundário numero 5 (E) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0356:
        return "Circuito Primário / Secundário numero 6 (F) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0357:
        return "Circuito Primário / Secundário numero 7 (G) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0358:
        return "Circuito Primário / Secundário numero 8 (H) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0359:
        return "Circuito Primário / Secundário numero 9 (I) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0360:
        return "Circuito Primário / Secundário numero 10 (J) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0361:
        return "Circuito Primário / Secundário numero 11 (K) com mau funcionamento da Bobina de Ignição";
      case FalhaEnum.P0362:
        return "Circuito Primário / Secundário numero 12 (L) com mau funcionamento da Bobina de Ignição";
      default:
        return "";
    }
  }

}