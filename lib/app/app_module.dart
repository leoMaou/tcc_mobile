import 'package:tcc_mobile/app/modules/diario_de_bordo/diario_de_bordo_page.dart';
import 'package:tcc_mobile/app/modules/eficiencia/eficiencia_controller.dart';
import 'package:tcc_mobile/app/modules/eficiencia/eficiencia_page.dart';
import 'package:tcc_mobile/app/modules/login/login_controller.dart';
import 'package:tcc_mobile/app/app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:tcc_mobile/app/app_widget.dart';
import 'package:tcc_mobile/app/modules/configuracao/configuracao_controller.dart';
import 'package:tcc_mobile/app/modules/configuracao/configuracao_page.dart';
import 'package:tcc_mobile/app/modules/login/login_module.dart';
import 'package:tcc_mobile/app/modules/pesquisar/pesquisar_controller.dart';
import 'package:tcc_mobile/app/modules/pesquisar/pesquisar_page.dart';
import 'package:tcc_mobile/app/modules/rastreamento/rastreamento_page.dart';
import 'package:tcc_mobile/app/modules/diario_de_bordo/diario_de_bordo_controller.dart';
import 'package:tcc_mobile/app/modules/rastreamento/rastreamento_controller.dart';
import 'package:tcc_mobile/app/repositories/configuracao_interface.dart';
import 'package:tcc_mobile/app/repositories/configuracao_repository.dart';
import 'package:tcc_mobile/app/repositories/falha_detectada_interface.dart';
import 'package:tcc_mobile/app/repositories/falha_detectada_repository.dart';

import 'modules/monitoramento/monitoramento_controller.dart';
import 'modules/monitoramento/monitoramento_page.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => LoginController(i.get())),
        Bind((i) => AppController()),
        Bind((i) => ConfiguracaoController(i.get())),
        Bind((i) => MonitoramentoController(i.get())),
        Bind((i) => EficienciaController()),
        Bind((i) => DiarioDeBordoController()),
        Bind((i) => PesquisarController()),
        Bind((i) => RastreamentoController()),
        Bind<IFalhaDectadaRepository>((i) => FalhaDetectadaRepository()),
        Bind<IConfiguracaoRepository>((i) => ConfiguracaoRepository())
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, module: LoginModule()),
        Router('/configuracao/', child: (_, args) => ConfiguracaoPage(), transition: TransitionType.fadeIn),
        Router('/monitoramento/', child: (_, args) => MonitoramentoPage(), transition: TransitionType.fadeIn),
        Router('/eficiencia/', child: (_, args) => EficienciaPage(), transition: TransitionType.fadeIn),
        Router('/diario_de_bordo/', child: (_, args) => DiarioDeBordoPage(), transition: TransitionType.fadeIn),
        Router('/pesquisar/', child: (_, args) => PesquisarPage(), transition: TransitionType.fadeIn),
        Router('/rastreamento/', child: (_, args) => RastreamentoPage(), transition: TransitionType.fadeIn),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
