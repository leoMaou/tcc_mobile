import 'dart:async';

import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/models/enum/comando_obd_enum.dart';
import 'package:tcc_mobile/app/models/falha_detectada_model.dart';
import 'package:tcc_mobile/app/repositories/falha_detectada_interface.dart';
import 'package:tcc_mobile/app/utils/bluetooth_provider.dart';

part 'monitoramento_controller.g.dart';

class MonitoramentoController = _MonitoramentoControllerBase
    with _$MonitoramentoController;

abstract class _MonitoramentoControllerBase with Store {
  final IFalhaDectadaRepository falhaDectadaRepository;
  final BluetoothProvider bluetoothProvider = BluetoothProvider.instance;

  _MonitoramentoControllerBase(this.falhaDectadaRepository);

  @observable
  ObservableList<BluetoothDiscoveryResult> results =
      ObservableList<BluetoothDiscoveryResult>();

  @observable
  bool isDiscovering;

  @observable
  bool isSearchingCode = false;

  @observable
  BluetoothDevice deviceConnected;

  @observable
  StreamSubscription<BluetoothDiscoveryResult> discoveryStreamSubscription;

  @observable
  int qtdFalhas = 0;

  @observable
  ObservableList<FalhaDetectadaModel> falhasList =
      ObservableList<FalhaDetectadaModel>();

  bool isSendingCommand = true;

  List<ObdComands> listCommandsToSend = new List();
  List<ObdComands> listCommandsAlreadySend = new List();

  int indexComandoAtual = 0;

  @action
  getFalhas() async {
//    falhasList = falhaDectadaRepository.getFalhas();
//    qtdFalhas = falhasList.length;

    listCommandsToSend = new List();
    listCommandsAlreadySend = new List();
    this.adicionarComandosIniciais();

    if (!listCommandsToSend.contains(ObdComands.VERIFICAR_SE_EXISTEM_FALHAS)) {
      listCommandsToSend.add(ObdComands.VERIFICAR_SE_EXISTEM_FALHAS);
    }
    this.isSearchingCode = true;
    startSendComands();
  }

  bool limparCodigos() {
    listCommandsToSend = new List();
    listCommandsAlreadySend = new List();
    this.adicionarComandosIniciais();
    if (!listCommandsToSend
        .contains(ObdComands.LIMPAR_CODIGOS_DE_PROBLEMAS_E_VALORES_SALVOS)) {
      listCommandsToSend
          .add(ObdComands.LIMPAR_CODIGOS_DE_PROBLEMAS_E_VALORES_SALVOS);
    }

    if (!listCommandsToSend.contains(ObdComands.BUCAS_FALHAS_DO_VEICULO)) {
      listCommandsToSend.add(ObdComands.BUCAS_FALHAS_DO_VEICULO);
    }
    this.isSearchingCode = true;
    startSendComands();
    return true;
  }

  adicionarComandosIniciais() {
    if (!listCommandsToSend.contains(ObdComands.RESETAR)) {
      listCommandsToSend.add(ObdComands.RESETAR);
    }
    if (!listCommandsToSend.contains(ObdComands.SETAR_PADRAO)) {
      listCommandsToSend.add(ObdComands.SETAR_PADRAO);
    }
    if (!listCommandsToSend
        .contains(ObdComands.SETAR_MELHOR_PROTOCOLO_PARA_LEITURA)) {
      listCommandsToSend.add(ObdComands.SETAR_MELHOR_PROTOCOLO_PARA_LEITURA);
    }
  }

  startSendComands() {
    indexComandoAtual = 0;
    bluetoothProvider.sendCommand(
        ObdComandsHelper.getValue(listCommandsToSend[indexComandoAtual]));
  }

  @action
  scanDispositivos() {
    isDiscovering = true;
    bluetoothProvider.scanDispositivos(results).onDone(() async {
      isDiscovering = false;
    });
  }

  @action
  conectarDispositivo(BluetoothDiscoveryResult resultDevice) async {
    String fullMessageCallback = "";
    // precisa colocar um delay, caso o comando retorne ele mesmo ou ?, dessa forma o comando será solicitado de novo;
    Duration durationDefault = new Duration(milliseconds: 1000);
    this.deviceConnected =
        await bluetoothProvider.conectarDispositivo(resultDevice, (data) {
      fullMessageCallback = fullMessageCallback + data.toString();

      if (fullMessageCallback.toString().contains(">") &&
          !fullMessageCallback.toString().contains("?")) {
        if (listCommandsToSend.length > indexComandoAtual) {
          print(listCommandsToSend[indexComandoAtual]);
        }
        print("Mensagem retornou com successo pelo obd");
        print("DATA FROM CALLBACK: " + fullMessageCallback);

        switch (listCommandsToSend[indexComandoAtual]) {
          case ObdComands.VERIFICAR_SE_EXISTEM_FALHAS:
            this.parseVerficarExistenciaFalhas(fullMessageCallback);
            break;
          case ObdComands.BUCAS_FALHAS_DO_VEICULO:
            this.parseGetFalhas(fullMessageCallback);
            break;
          case ObdComands.LIMPAR_CODIGOS_DE_PROBLEMAS_E_VALORES_SALVOS:
            break;
          default:
            break;
        }
        fullMessageCallback = "";
        if (listCommandsToSend.length > indexComandoAtual) {
          listCommandsAlreadySend.add(listCommandsToSend[indexComandoAtual]);
          indexComandoAtual++;
          if (listCommandsToSend.length > indexComandoAtual) {
            bluetoothProvider.sendCommand(ObdComandsHelper.getValue(
                listCommandsToSend[indexComandoAtual]));
          }
        }
      } else {
        fullMessageCallback = "";
        Future.delayed(durationDefault, () {
          //se ainda tem comando para rodar
          if (listCommandsToSend.length > indexComandoAtual) {
            //se o comando não retornou nada ainda
            if (!listCommandsAlreadySend
                .contains(listCommandsToSend[indexComandoAtual])) {
              //roda o comando de novo
              bluetoothProvider.sendCommand(ObdComandsHelper.getValue(
                  listCommandsToSend[indexComandoAtual]));
            }
          }
        });
      }
    });
    if (this.deviceConnected != null) {
      return true;
    }
    return false;
  }

  parseGetFalhas(String rawDtc) {
    //remove os espaços em branco e o simbolo de maior>
    String dtc = rawDtc
        .toString()
        .trim()
        .replaceAll("\n", "")
        .replaceAll("\r", "")
        .replaceAll("\t", "")
        .replaceAll(">", "");
    if (!dtc.contains("0343 00 ")) {
      //remove os primeiros 8 caracteres correspondentes ao comando executado
      String avaria = dtc.substring(8).replaceAll(" ", "");
      // pega o primeiro charactere para adquirir os primeiros bytes do codigo
      String firstCharacter =
          this.parseDTCFirstCharacter(avaria.substring(0, 1));
      if (firstCharacter != null) {
        //concatena o primeiro caractere substituindo o 1 valor da falha trazida
        String codigoParsed = firstCharacter + avaria.substring(1);
        print("codigo de avaria detectado: " + codigoParsed);
        FalhaDetectadaModel falha = new FalhaDetectadaModel(codigoParsed);

        bool alreadyExist = false;
        for (var i = 0; i < this.falhasList.length; i++) {
          if (falha.codigo == this.falhasList[i].codigo) {
            alreadyExist = true;
          }
        }

        if (!alreadyExist) {
          this.falhasList.add(falha);
        }
      }
    } else {
      this.qtdFalhas = 0;
      this.falhasList.clear();
    }
    this.isSearchingCode = false;
  }

  parseVerficarExistenciaFalhas(String rawDtc) {
    //remove os espaços em branco e o simbolo de maior>
    String dtc = rawDtc
        .toString()
        .trim()
        .replaceAll("SEARCHING...", "")
        .replaceAll("\r", "")
        .replaceAll("\n", "")
        .replaceAll("\t", "")
        .replaceAll(">", "");

    //remove os primeiros 10 caracteres correspondentes ao comando executado
    dtc = dtc.substring(10);

    // pega o hexadecimal dos 2 primeiros bits do dtc
    String hex = dtc.substring(0, 2);

    //converte em binario
    String Abinario = int.parse(hex, radix: 16).toRadixString(2);

    //pega o valor para saber se a luz de avaria está ligada ou não
    String luzAvaria = Abinario.substring(0, 1);

    //pega o binario correspondente a quantidade de erros detectados
    String quantidadeAvariaBinario = Abinario.substring(1);
    print("Binario Luz de avaria: " + luzAvaria);

    if (quantidadeAvariaBinario != "") {
      // converte o binario da quantidade de avaria para decimal
      int quantidadeAvaria = int.parse(quantidadeAvariaBinario, radix: 2);

      if (quantidadeAvaria > 0) {
        print("FALHAS ENCONTADAS: " + quantidadeAvaria.toString());
        //seta valor para o observable
        this.qtdFalhas = quantidadeAvaria;

        if (!listCommandsToSend.contains(ObdComands.BUCAS_FALHAS_DO_VEICULO)) {
          listCommandsToSend.add(ObdComands.BUCAS_FALHAS_DO_VEICULO);
        }
      }
    } else {
      this.qtdFalhas = 0;
      this.falhasList.clear();
      this.isSearchingCode = false;
    }
  }

  String parseDTCFirstCharacter(String firstCharacter) {
    switch (firstCharacter) {
      case "0":
        return "P0";
      case "1":
        return "P1";
      case "3":
        return "P3";
      case "4":
        return "C0";
      case "5":
        return "C1";
      case "6":
        return "C2";
      case "7":
        return "C3";
      case "8":
        return "B0";
      case "9":
        return "B1";
      case "A":
        return "B2";
      case "B":
        return "B3";
      case "C":
        return "U0";
      case "D":
        return "U1";
      case "E":
        return "U2";
      case "F":
        return "U3";
      default:
        return null;
        break;
    }
  }

  @action
  desconectarDispositivo() {
    bluetoothProvider.desconectarDispositivo();
  }

  @action
  pararListenerScan() async {
    await bluetoothProvider.stopScanListener();
  }
}
