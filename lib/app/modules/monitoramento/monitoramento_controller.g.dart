// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'monitoramento_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MonitoramentoController on _MonitoramentoControllerBase, Store {
  final _$resultsAtom = Atom(name: '_MonitoramentoControllerBase.results');

  @override
  ObservableList<BluetoothDiscoveryResult> get results {
    _$resultsAtom.reportRead();
    return super.results;
  }

  @override
  set results(ObservableList<BluetoothDiscoveryResult> value) {
    _$resultsAtom.reportWrite(value, super.results, () {
      super.results = value;
    });
  }

  final _$isDiscoveringAtom =
      Atom(name: '_MonitoramentoControllerBase.isDiscovering');

  @override
  bool get isDiscovering {
    _$isDiscoveringAtom.reportRead();
    return super.isDiscovering;
  }

  @override
  set isDiscovering(bool value) {
    _$isDiscoveringAtom.reportWrite(value, super.isDiscovering, () {
      super.isDiscovering = value;
    });
  }

  final _$isSearchingCodeAtom =
      Atom(name: '_MonitoramentoControllerBase.isSearchingCode');

  @override
  bool get isSearchingCode {
    _$isSearchingCodeAtom.reportRead();
    return super.isSearchingCode;
  }

  @override
  set isSearchingCode(bool value) {
    _$isSearchingCodeAtom.reportWrite(value, super.isSearchingCode, () {
      super.isSearchingCode = value;
    });
  }

  final _$deviceConnectedAtom =
      Atom(name: '_MonitoramentoControllerBase.deviceConnected');

  @override
  BluetoothDevice get deviceConnected {
    _$deviceConnectedAtom.reportRead();
    return super.deviceConnected;
  }

  @override
  set deviceConnected(BluetoothDevice value) {
    _$deviceConnectedAtom.reportWrite(value, super.deviceConnected, () {
      super.deviceConnected = value;
    });
  }

  final _$discoveryStreamSubscriptionAtom =
      Atom(name: '_MonitoramentoControllerBase.discoveryStreamSubscription');

  @override
  StreamSubscription<BluetoothDiscoveryResult> get discoveryStreamSubscription {
    _$discoveryStreamSubscriptionAtom.reportRead();
    return super.discoveryStreamSubscription;
  }

  @override
  set discoveryStreamSubscription(
      StreamSubscription<BluetoothDiscoveryResult> value) {
    _$discoveryStreamSubscriptionAtom
        .reportWrite(value, super.discoveryStreamSubscription, () {
      super.discoveryStreamSubscription = value;
    });
  }

  final _$qtdFalhasAtom = Atom(name: '_MonitoramentoControllerBase.qtdFalhas');

  @override
  int get qtdFalhas {
    _$qtdFalhasAtom.reportRead();
    return super.qtdFalhas;
  }

  @override
  set qtdFalhas(int value) {
    _$qtdFalhasAtom.reportWrite(value, super.qtdFalhas, () {
      super.qtdFalhas = value;
    });
  }

  final _$falhasListAtom =
      Atom(name: '_MonitoramentoControllerBase.falhasList');

  @override
  ObservableList<FalhaDetectadaModel> get falhasList {
    _$falhasListAtom.reportRead();
    return super.falhasList;
  }

  @override
  set falhasList(ObservableList<FalhaDetectadaModel> value) {
    _$falhasListAtom.reportWrite(value, super.falhasList, () {
      super.falhasList = value;
    });
  }

  final _$getFalhasAsyncAction =
      AsyncAction('_MonitoramentoControllerBase.getFalhas');

  @override
  Future getFalhas() {
    return _$getFalhasAsyncAction.run(() => super.getFalhas());
  }

  final _$conectarDispositivoAsyncAction =
      AsyncAction('_MonitoramentoControllerBase.conectarDispositivo');

  @override
  Future conectarDispositivo(BluetoothDiscoveryResult resultDevice) {
    return _$conectarDispositivoAsyncAction
        .run(() => super.conectarDispositivo(resultDevice));
  }

  final _$pararListenerScanAsyncAction =
      AsyncAction('_MonitoramentoControllerBase.pararListenerScan');

  @override
  Future pararListenerScan() {
    return _$pararListenerScanAsyncAction.run(() => super.pararListenerScan());
  }

  final _$_MonitoramentoControllerBaseActionController =
      ActionController(name: '_MonitoramentoControllerBase');

  @override
  dynamic scanDispositivos() {
    final _$actionInfo = _$_MonitoramentoControllerBaseActionController
        .startAction(name: '_MonitoramentoControllerBase.scanDispositivos');
    try {
      return super.scanDispositivos();
    } finally {
      _$_MonitoramentoControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic desconectarDispositivo() {
    final _$actionInfo =
        _$_MonitoramentoControllerBaseActionController.startAction(
            name: '_MonitoramentoControllerBase.desconectarDispositivo');
    try {
      return super.desconectarDispositivo();
    } finally {
      _$_MonitoramentoControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
results: ${results},
isDiscovering: ${isDiscovering},
isSearchingCode: ${isSearchingCode},
deviceConnected: ${deviceConnected},
discoveryStreamSubscription: ${discoveryStreamSubscription},
qtdFalhas: ${qtdFalhas},
falhasList: ${falhasList}
    ''';
  }
}
