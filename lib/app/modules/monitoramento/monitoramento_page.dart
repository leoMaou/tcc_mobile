import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tcc_mobile/app/models/falha_detectada_model.dart';
import 'package:tcc_mobile/app/utils/side_menu.dart';
import 'monitoramento_controller.dart';

class MonitoramentoPage extends StatefulWidget {
  final String title;

  const MonitoramentoPage({Key key, this.title = "Monitoramento"})
      : super(key: key);

  @override
  _MonitoramentoPageState createState() => _MonitoramentoPageState();
}

class _MonitoramentoPageState
    extends ModularState<MonitoramentoPage, MonitoramentoController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(child: Text("Resumo")),
              Tab(child: Text("Testes")),
              Tab(child: Text("Tempo Real")),
            ],
          ),
          title: Text(widget.title),
        ),
        drawer: new Drawer(
          child: Side_menu(context),
        ),
        body: TabBarView(
          children: [
            pageResumo(context),
            pageTestes(context),
            pageTempoReal(context)
          ],
        ),
      ),
    );
  }

  Widget pageResumo(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: new SingleChildScrollView(
            child: new Column(children: <Widget>[
          Container(
              child: Center(
                  child: Text("Falhas",
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.bold)))),
          Container(child: Observer(builder: (_) {
            return Center(
                child: Text("${controller.qtdFalhas}",
                    style: TextStyle(
                        fontSize: 60,
                        fontWeight: FontWeight.bold,
                        color: Colors.red)));
          })),
          Container(height: 1.0, width: 250, color: Colors.black),
          Container(
              padding: const EdgeInsets.all(10.0),
              child: Center(
                  child: Text("Detalhes",
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.bold)))),
          Container(
              height: 190,
              child: Observer(
                builder: (_) {

                  if(controller.isSearchingCode) {
                    return Center(child: CircularProgressIndicator());
                  }

                  if (controller.falhasList.length == 0) {
                    return Center(child: Text("Nenhuma falha encontrada"));
                  }
                  return ListView.builder(
                      itemCount: controller.falhasList.length,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (_, index) {
                        FalhaDetectadaModel model =
                            controller.falhasList[index];
                        return ListTile(
                          title: Text(model.codigo,
                              style: TextStyle(
                              fontWeight: FontWeight.bold)),
                          subtitle: Text(model.descricao),
                        );
                      });
                },
              )),
          Container(height: 1.0, width: 250, color: Colors.black),
          Container(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
            alignment: Alignment.center,
            child: Observer(
              builder: (_) {
                return RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  onPressed: controller.deviceConnected == null ? null : () {
                          controller.getFalhas();
                        },
                  child: Text("Buscar Falhas"),
                );
              },
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Observer(builder: (_) {
                  String textButton = "Limpar código de avaria";

                  /*if (controller.qtdFalhas == 0) {
                    return RaisedButton(
                      textColor: Colors.white,
                      color: Colors.blue,
                      onPressed: null,
                      child: Text(textButton),
                    );
                  }*/

                  return RaisedButton(
                    textColor: Colors.white,
                    color: Colors.blue,
                    onPressed: controller.deviceConnected == null ? null : () {
                      if (controller.limparCodigos()) {
                        _showToast("Limpado com sucesso");
                      } else {
                        _showToast(
                            "Não foi possivel limpar os codigos de avaria");
                      }
                    },
                    child: Text(textButton),
                  );
                }),
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  onPressed: () {
                    controller.scanDispositivos();
                    _showBluetoothDialog();
                   /* String falha = "0101\n\t41 01 81 07 E1 E1\n\t>";
                    controller.parseVerficarExistenciaFalhas(falha);*/
                  },
                  child: Text("Conectar ao OBD"),
                ),
              ],
            ),
          ),
        ])));
  }

  Widget pageTestes(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: new SingleChildScrollView(
            child: new Column(children: <Widget>[
          Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: Center(
                  child: Text("Testes realizados no veículo",
                      style: TextStyle(
                          fontSize: 25, fontWeight: FontWeight.bold)))),
          Container(height: 1.0, width: 300, color: Colors.black),
          Container(
              height: 350,
              child: Center(
                  child:
                      Text("Listagem de testes realizados não implementada"))),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  onPressed: () {},
                  child: Text("Realizar testes"),
                ),
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  onPressed: () {},
                  child: Text("Exportar testes"),
                ),
              ],
            ),
          )
        ])));
  }

  Widget pageTempoReal(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: new SingleChildScrollView(
            child: new Column(children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  onPressed: () {},
                  child: Text("Iniciar"),
                ),
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  onPressed: null,
                  child: Text("Reiniciar"),
                ),
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  onPressed: null,
                  child: Text("Parar"),
                )
              ],
            ),
          ),
          Container(
              height: 350,
              child: Center(
                  child: Text(
                      "Listagem de testes em tempo real não implementada"))),
        ])));
  }

  void _showToast(String msg) {
    Fluttertoast.showToast(msg: msg);
  }

  _showBluetoothDialog() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title:
                Text("Selecione o dispositivo", style: TextStyle(fontSize: 15)),
            content: new SingleChildScrollView(
                child: new Column(children: <Widget>[
              Container(
                  height: 250,
                  width: double.maxFinite,
                  child: Observer(
                    builder: (_) {
                      if (controller.isDiscovering) {
                        return Center(child: CircularProgressIndicator());
                      } else if (controller.results.length == 0) {
                        return Center(child: Text("Nada encontrado"));
                      }
                      return ListView.builder(
                          itemCount: controller.results.length,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (_, index) {
                            BluetoothDiscoveryResult result =
                                controller.results[index];
                            return Observer(
                              builder: (_) {
                                return ListTile(
                                    title: Text(
                                        result.device.name != ''
                                            ? result.device.name
                                            : '(dispositivo desconhecido)',
                                        style: TextStyle(fontSize: 15)),
                                    subtitle: Text(result.device.address +
                                        result.rssi.toString()),
                                    trailing: Text(result.device ==
                                            controller.deviceConnected
                                        ? "Conectado"
                                        : "Conectar"),
                                    onTap:  result.device == controller.deviceConnected ? null : () async {

                                        print(result.device.name);
                                        bool isconnected = await controller.conectarDispositivo(result);
                                        if(isconnected) {
                                          _showToast("Conectado com sucesso");
                                          Navigator.of(context).pop();
                                        }else {
                                          _showToast("Não foi possivel se conectar");
                                        }

                                    });
                              },
                            );
                          });
                    },
                  )),
              RaisedButton(
                textColor: Colors.white,
                color: Colors.blue,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("Sair"),
              )
            ])),
          );
        }).then((value) {
          controller.pararListenerScan();
    });
  }
}
