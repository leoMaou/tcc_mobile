import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/monitoramento/monitoramento_controller.dart';
import 'package:tcc_mobile/app/modules/monitoramento/monitoramento_page.dart';
import 'package:tcc_mobile/app/repositories/falha_detectada_interface.dart';
import 'package:tcc_mobile/app/repositories/falha_detectada_repository.dart';

class MonitoramentoModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => MonitoramentoController(i.get())),
        Bind<IFalhaDectadaRepository>((i) => FalhaDetectadaRepository())
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => MonitoramentoPage()),
      ];

  static Inject get to => Inject<MonitoramentoModule>.of();
}
