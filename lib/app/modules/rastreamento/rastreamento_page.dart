import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/utils/side_menu.dart';
import 'rastreamento_controller.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class RastreamentoPage extends StatefulWidget {
  final String title;

  const RastreamentoPage({Key key, this.title = "Rastreamento por GPS"})
      : super(key: key);

  @override
  _RastreamentoPageState createState() => _RastreamentoPageState();
}

class _RastreamentoPageState
    extends ModularState<RastreamentoPage, RastreamentoController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: new Drawer(
          child: Side_menu(context),
        ),
        body: GoogleMap(
          onMapCreated: controller.onMapCreated,
          initialCameraPosition: CameraPosition(
            target: controller.center,
            zoom: 16.0,
          ),
        ),);
  }
}
