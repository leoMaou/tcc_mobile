import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobx/mobx.dart';

part 'rastreamento_controller.g.dart';

class RastreamentoController = _RastreamentoControllerBase with _$RastreamentoController;

abstract class _RastreamentoControllerBase with Store {
  GoogleMapController mapController;

  final LatLng center = const LatLng(-28.94611948, -49.46122169);

  void onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

}
