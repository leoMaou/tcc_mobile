import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/utils/side_menu.dart';
import 'pesquisar_controller.dart';

class PesquisarPage extends StatefulWidget {
  final String title;
  const PesquisarPage({Key key, this.title = "Pesquisar Códigos"}) : super(key: key);

  @override
  _PesquisarPageState createState() => _PesquisarPageState();
}

class _PesquisarPageState
    extends ModularState<PesquisarPage, PesquisarController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
        drawer: new Drawer(
          child: Side_menu(context),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: new SingleChildScrollView(
                child: new Column(children: <Widget>[
                  Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: "Digite o codigo para pesquisar..."
                        ),
                        onChanged: (texto) {
                          controller.pesquisarCodigo(texto);
                        },
                      )
                  ),
                ]))));
  }
}
