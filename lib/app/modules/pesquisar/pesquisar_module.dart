import 'pesquisar_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';

class PesquisarModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => PesquisarController()),
      ];

  @override
  List<Router> get routers => [];

  static Inject get to => Inject<PesquisarModule>.of();
}
