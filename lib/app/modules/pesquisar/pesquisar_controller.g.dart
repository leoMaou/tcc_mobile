// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pesquisar_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PesquisarController on _PesquisarControllerBase, Store {
  final _$_PesquisarControllerBaseActionController =
      ActionController(name: '_PesquisarControllerBase');

  @override
  void pesquisarCodigo(String texto) {
    final _$actionInfo = _$_PesquisarControllerBaseActionController.startAction(
        name: '_PesquisarControllerBase.pesquisarCodigo');
    try {
      return super.pesquisarCodigo(texto);
    } finally {
      _$_PesquisarControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
