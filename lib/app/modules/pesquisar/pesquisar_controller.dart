import 'package:mobx/mobx.dart';

part 'pesquisar_controller.g.dart';

class PesquisarController = _PesquisarControllerBase with _$PesquisarController;

abstract class _PesquisarControllerBase with Store {

  @action
  void pesquisarCodigo(String texto) {
    //todo pesquisa de codigo
  }
}
