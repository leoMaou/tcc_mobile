import 'package:mobx/mobx.dart';

part 'diario_de_bordo_controller.g.dart';

class DiarioDeBordoController = _DiarioDeBordoControllerBase
    with _$DiarioDeBordoController;

abstract class _DiarioDeBordoControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
