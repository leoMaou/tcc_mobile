import 'diario_de_bordo_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';

class DiarioDeBordoModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => DiarioDeBordoController()),
      ];

  @override
  List<Router> get routers => [];

  static Inject get to => Inject<DiarioDeBordoModule>.of();
}
