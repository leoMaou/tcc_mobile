import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/utils/side_menu.dart';
import 'diario_de_bordo_controller.dart';

class DiarioDeBordoPage extends StatefulWidget {
  final String title;
  const DiarioDeBordoPage({Key key, this.title = "Diário de Bordo"})
      : super(key: key);

  @override
  _DiarioDeBordoPageState createState() => _DiarioDeBordoPageState();
}

class _DiarioDeBordoPageState extends ModularState<DiarioDeBordoPage, DiarioDeBordoController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
        drawer: new Drawer(
          child: Side_menu(context),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: new SingleChildScrollView(
                child: new Column(children: <Widget>[
                  Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                      child: Center(
                          child: Text("Percursos anteriores",
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold)))),
                  Container(height: 1.0, width: 300, color: Colors.black),
                  Container(
                      height: 400,
                      child: Center(child: Text("Listagem de percursos não implementada")
                      )),
                ]))));
  }
}
