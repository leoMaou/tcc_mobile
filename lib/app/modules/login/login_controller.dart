import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/models/configuracao_model.dart';
import 'package:tcc_mobile/app/repositories/configuracao_interface.dart';

part 'login_controller.g.dart';

class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {

  final IConfiguracaoRepository configuracaoRepository;

  _LoginControllerBase(this.configuracaoRepository);

  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }

  Future<bool> existConfig() async{
    int countConfig = await configuracaoRepository.getCount();
    if(countConfig > 0) {
      return true;
    }else {
      return false;
    }
  }


}
