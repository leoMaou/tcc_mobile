import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/configuracao/configuracao_controller.dart';
import 'package:tcc_mobile/app/modules/login/login_controller.dart';

class LoginPage extends StatefulWidget {
  final String title;

  const LoginPage({Key key, this.title = "Login"}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    height: 200.0,
                    padding: EdgeInsets.all(10),
                    child: Image.asset(
                      "assets/images/unesc_LOGO.png",
                      fit: BoxFit.contain,
                    )),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Usuario',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Senha',
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    //forgot password screen
                  },
                  textColor: Colors.blue,
                  child: Text('Esqueci a minha senha'),
                ),
                Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.blue,
                      child: Text('Acessar'),
                      onPressed: () async {
                        bool exist = await controller.existConfig();
                        if(exist) {
                          Navigator.popAndPushNamed(context, '/monitoramento/');
                        }else {
                          Navigator.pushNamed(context, '/configuracao/');
                        }
                      },
                    )),
                Container(
                    child: Row(
                  children: <Widget>[
                    Text('Não possui conta?'),
                    FlatButton(
                      textColor: Colors.blue,
                      child: Text(
                        'Criar conta',
                        style: TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        //Tela de se cadastrar
                      },
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ))
              ],
            )));
  }
}
