import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/login/login_page.dart';
import 'package:tcc_mobile/app/repositories/configuracao_interface.dart';
import 'package:tcc_mobile/app/repositories/configuracao_repository.dart';
import 'login_controller.dart';

class LoginModule extends ChildModule {
  @override
  List<Bind> get binds => [
    Bind((i) => LoginController(i.get())),
    Bind<IConfiguracaoRepository>((i) => ConfiguracaoRepository())
  ];

  @override
  List<Router> get routers => [
    Router('/', child: (_, args) => LoginPage()),
  ];

  static Inject get to => Inject<LoginModule>.of();
}
