import 'package:mobx/mobx.dart';
import 'package:charts_flutter/flutter.dart' as charts;
part 'eficiencia_controller.g.dart';

class EficienciaController = _EficienciaControllerBase
    with _$EficienciaController;

abstract class _EficienciaControllerBase with Store {
  List<charts.Series> seriesList;
  bool animate;

  _EficienciaControllerBase() {
    seriesList = _createSampleData();
    animate = true;
  }

  }

  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }

  /// Create one series with sample hard coded data.
  List<charts.Series<LinearSales, int>> _createSampleData() {
    final data = [
      new LinearSales(0, 5),
      new LinearSales(1, 25),
      new LinearSales(2, 100),
      new LinearSales(3, 75),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }


/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}