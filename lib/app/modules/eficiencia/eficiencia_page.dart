import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/utils/side_menu.dart';
import 'eficiencia_controller.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class EficienciaPage extends StatefulWidget {
  final String title;

  const EficienciaPage({Key key, this.title = "Eficiencia"}) : super(key: key);

  @override
  _EficienciaPageState createState() => _EficienciaPageState();
}

class _EficienciaPageState
    extends ModularState<EficienciaPage, EficienciaController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: new Drawer(
          child: Side_menu(context),
        ),
        body: Padding(
            padding: EdgeInsets.all(10),
            child: new SingleChildScrollView(
                child: new Column(children: <Widget>[
                  Container(
                      padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                          child: Text("Tensão da bateria"))),
                  Container(
                      height: 100,
                      child: charts.LineChart(controller.seriesList, animate: controller.animate),
                      ),
                  Container(
                      padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text("Eficiencia do combustivel")
                      )),
                  Container(
                      height: 100,
                    child: charts.LineChart(controller.seriesList, animate: controller.animate)
                      ),
                  Container(
                      padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text("Torque x Potencia x RPM"))),
                  Container(
                      height: 100,
                    child: charts.LineChart(controller.seriesList, animate: controller.animate),
                  ),
                ]))));
  }

}

