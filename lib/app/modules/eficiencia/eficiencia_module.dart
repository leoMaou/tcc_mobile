import 'eficiencia_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';

class EficienciaModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => EficienciaController()),
      ];

  @override
  List<Router> get routers => [];

  static Inject get to => Inject<EficienciaModule>.of();
}
