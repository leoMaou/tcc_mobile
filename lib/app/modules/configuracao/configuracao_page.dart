import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'configuracao_controller.dart';

class ConfiguracaoPage extends StatefulWidget {
  final String title;

  const ConfiguracaoPage({Key key, this.title = "Configurações"})
      : super(key: key);

  @override
  _ConfiguracaoPageState createState() => _ConfiguracaoPageState();
}

class _ConfiguracaoPageState
    extends ModularState<ConfiguracaoPage, ConfiguracaoController> {

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
          padding: EdgeInsets.all(10),
          child: new SingleChildScrollView(
              child: new Column(
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Ano',
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha com o ano do veiculo';
                                  }
                                  try{
                                    int.parse(value);
                                  } catch(e){
                                    return 'Por favor preencha com o ano do veiculo valido';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.ano = value;
                                  }
                                },
                                controller: new TextEditingController(text: controller.ano),
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Fabricante',
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha com o fabricante do veiculo';
                                  }
                                  return null;
                                },
                                controller: new TextEditingController(text: controller.fabricante),
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.fabricante = value;
                                  }
                                },
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Tipo do veículo',
                                    suffixIcon: IconButton(
                                        icon: Icon(Icons.lightbulb_outline),
                                        onPressed: () {
                                          _showMessageDialog("Tipo do veículo",
                                              "Os tipos do veículo podem ser Sendan, SUV, Minivan, Van, Carro esportivo");
                                        })),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha com o Tipo do veículo';
                                  }
                                  return null;
                                },
                                controller: new TextEditingController(text: controller.tipo_veiculo),
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.tipo_veiculo = value;
                                  }
                                },
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Tipo de Combustivel',
                                    suffixIcon: IconButton(
                                        icon: Icon(Icons.lightbulb_outline),
                                        onPressed: () {
                                          _showMessageDialog("Combustivel",
                                              "Os Tipos de combustiveis são Gasolina, E85, E100, Diesel");
                                        })
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha com o Tipo de Combustivel do veículo';
                                  }
                                  return null;
                                },
                                controller: new TextEditingController(text: controller.combustivel),
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.combustivel = value;
                                  }
                                },
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Tamanho do motor (Litros)',
                                    suffixIcon: IconButton(
                                        icon: Icon(Icons.lightbulb_outline),
                                        onPressed: () {
                                          _showMessageDialog(
                                              "Tamanho do motor (Litros)",
                                              "Pode ser encontrada no manual do carro, é importante para o calculo da eficiencia do combustivel, força e torque. Isso se o veículo não possuir um sensor de fluxo de ar em massa ou se o cálculo da taxa de combustível for 'pressão absoluta do coletor de admissão'");
                                        })
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha com o Tamanho do motor em litros';
                                  }

                                  try{
                                    double.parse(value);
                                  } catch(e){
                                    return 'Por favor preencha com um numero valido';
                                  }

                                  return null;
                                },
                                controller: new TextEditingController(text: controller.tamanho_motor),
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.tamanho_motor = value;
                                  }
                                },
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Eficiência do motor',
                                    suffixIcon: IconButton(
                                        icon: Icon(Icons.lightbulb_outline),
                                        onPressed: () {
                                          _showMessageDialog(
                                              "Eficiência do motor",
                                              "Normalmente entre 65% a 85%. É utilizado para calcular o torque, potencia e eficiencia do combustivel.");
                                        })
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha com a porcentagem de Eficiência do motor';
                                  }
                                  try{
                                    double.parse(value);
                                  } catch(e){
                                    return 'Por favor preencha com um numero valido';
                                  }
                                  return null;
                                },
                                controller: new TextEditingController(text: controller.eficiencia_motor),
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.eficiencia_motor = value;
                                  }
                                },
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Consumo do combustível',
                                    suffixIcon: IconButton(
                                        icon: Icon(Icons.lightbulb_outline),
                                        onPressed: () {
                                          _showMessageDialog(
                                              "Consumo do combustível",
                                              "Normalmente entre 0.40 e 0.60, mas para indução forçada 0.5 e 0.6, mas para diesel 0.30 e 0.40");
                                        })
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha com Consumo do combustível do veículo';
                                  }
                                  try{
                                    double.parse(value);
                                  } catch(e){
                                    return 'Por favor preencha com um numero valido';
                                  }
                                  return null;
                                },
                                controller: new TextEditingController(text: controller.consumo_combustivel),
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.consumo_combustivel = value;
                                  }
                                },
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Capacidade do tanque de combustível (gal)',
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha com a Capacidade do tanque de combustível do veículo em galões';
                                  }
                                  try{
                                    double.parse(value);
                                  } catch(e){
                                    return 'Por favor preencha com um numero valido';
                                  }
                                  return null;
                                },
                                controller: new TextEditingController(text: controller.capacidade_tanque),
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.capacidade_tanque = value;
                                  }
                                },
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Custo do combustivel por unidade (Reais R\$)',
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha o valor do combustivel';
                                  }
                                  try{
                                    double.parse(value);
                                  } catch(e){
                                    return 'Por favor preencha com um numero valido';
                                  }
                                  return null;
                                },
                                controller: new TextEditingController(text: controller.custo_unidade),
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.custo_unidade = value;
                                  }
                                },
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Observer(builder: (_) {
                              return TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Fator de escala da velocidade do veículo',
                                    suffixIcon: IconButton(
                                        icon: Icon(Icons.lightbulb_outline),
                                        onPressed: () {
                                          _showMessageDialog(
                                              "Fator de escala da velocidade do veículo",
                                              "velocidade de leitura do scanner: 1 significa calibração, 0.9 significa lento, e 1.1 significa rapido");
                                        })
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Por favor preencha o Fator de escala da velocidade do veículo';
                                  }
                                  try{
                                    double.parse(value);
                                  } catch(e){
                                    return 'Por favor preencha com um numero valido';
                                  }
                                  return null;
                                },
                                controller: new TextEditingController(text: controller.fator_escala),
                                onSaved: (value) {
                                  if(value != null) {
                                    controller.fator_escala = value;
                                  }
                                },
                              );
                            },)
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            alignment: Alignment.centerRight,
                            child: RaisedButton(
                              textColor: Colors.white,
                              color: Colors.blue,
                              onPressed: () {
                                final form = _formKey.currentState;
                                if(form.validate()) {
                                  form.save();
                                  controller.saveOrUpdateConfig();
                                  Navigator.popAndPushNamed(context, '/monitoramento/');
                                }
                              },
                              child: Text(
                                "Salvar",
                                style: TextStyle(fontSize: 20),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ]
              )
          )
      ),
    );
  }

  _showMessageDialog(String title, String content) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            title: Text(title),
            content: Text(content),
            actions: <Widget>[
              FlatButton(onPressed: () {
                Modular.to.pop();
              }, child: Text("OK"))
            ],
          );
        }
    );
  }

}
