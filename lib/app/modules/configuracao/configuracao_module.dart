import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/configuracao/configuracao_page.dart';
import 'package:tcc_mobile/app/modules/configuracao/configuracao_controller.dart';
import 'package:tcc_mobile/app/repositories/configuracao_interface.dart';
import 'package:tcc_mobile/app/repositories/configuracao_repository.dart';

class ConfiguracaoModule extends ChildModule {
  @override
  List<Bind> get binds => [
    Bind((i) => ConfiguracaoController(i.get())),
    Bind<IConfiguracaoRepository>((i) => ConfiguracaoRepository())
  ];

  @override
  List<Router> get routers => [
    Router('/', child: (_, args) => ConfiguracaoPage()),
  ];

  static Inject get to => Inject<ConfiguracaoModule>.of();
}
