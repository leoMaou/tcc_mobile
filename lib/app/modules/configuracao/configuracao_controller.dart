import 'dart:async';

import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/models/configuracao_model.dart';
import 'package:tcc_mobile/app/repositories/configuracao_interface.dart';
import 'package:tcc_mobile/app/utils/bluetooth_provider.dart';

part 'configuracao_controller.g.dart';

class ConfiguracaoController = _ConfiguracaoControllerBase with _$ConfiguracaoController;

abstract class _ConfiguracaoControllerBase with Store {
  final IConfiguracaoRepository configuracaoRepository;

  _ConfiguracaoControllerBase(this.configuracaoRepository) {
    getConfig();
  }

  @observable
  int id;
  @observable
  String ano = "";
  @observable
  String fabricante = "";
  @observable
  String tipo_veiculo = "Hatch";
  @observable
  String combustivel = "Gasolina";
  @observable
  String tamanho_motor = "2";
  @observable
  String eficiencia_motor = "65";
  @observable
  String consumo_combustivel = "0.45";
  @observable
  String capacidade_tanque = "14";
  @observable
  String custo_unidade = "4.10";
  @observable
  String fator_escala = "1";

  @action
  getConfig() async{
    ConfiguracaoModel configuracaoLoaded = await configuracaoRepository.findById(1);
    if(configuracaoLoaded != null) {
      this.id = configuracaoLoaded.id;
      this.ano = configuracaoLoaded.ano.toString();
      this.fabricante = configuracaoLoaded.fabricante;
      this.tipo_veiculo = configuracaoLoaded.tipo_veiculo;
      this.combustivel = configuracaoLoaded.combustivel;
      this.tamanho_motor = configuracaoLoaded.tamanho_motor;
      this.eficiencia_motor = configuracaoLoaded.eficiencia_motor;
      this.consumo_combustivel = configuracaoLoaded.consumo_combustivel.toString();
      this.capacidade_tanque = configuracaoLoaded.capacidade_tanque.toString();
      this.custo_unidade = configuracaoLoaded.custo_unidade.toString();
      this.fator_escala = configuracaoLoaded.fator_escala.toString();
    }
  }

  @action
  saveOrUpdateConfig() async {
    ConfiguracaoModel configuracao = new ConfiguracaoModel(ano: int.parse(this.ano),
        tipo_veiculo: this.tipo_veiculo,combustivel: this.combustivel,tamanho_motor: this.tamanho_motor, eficiencia_motor:
        this.eficiencia_motor, consumo_combustivel: double.parse(this.consumo_combustivel),capacidade_tanque:  double.parse(this.capacidade_tanque),
        custo_unidade: double.parse(this.custo_unidade), fator_escala: double.parse(this.fator_escala), fabricante: this.fabricante);

    if(this.id == null) {
      print('Inserindo config no banco');
      this.id = (await configuracaoRepository.insert(configuracao));
    }else {
      configuracao.id = this.id;
      print('Atualizando config no banco');
      await configuracaoRepository.update(configuracao);
    }
  }
}
