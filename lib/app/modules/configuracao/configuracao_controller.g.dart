// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'configuracao_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ConfiguracaoController on _ConfiguracaoControllerBase, Store {
  final _$idAtom = Atom(name: '_ConfiguracaoControllerBase.id');

  @override
  int get id {
    _$idAtom.reportRead();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.reportWrite(value, super.id, () {
      super.id = value;
    });
  }

  final _$anoAtom = Atom(name: '_ConfiguracaoControllerBase.ano');

  @override
  String get ano {
    _$anoAtom.reportRead();
    return super.ano;
  }

  @override
  set ano(String value) {
    _$anoAtom.reportWrite(value, super.ano, () {
      super.ano = value;
    });
  }

  final _$fabricanteAtom = Atom(name: '_ConfiguracaoControllerBase.fabricante');

  @override
  String get fabricante {
    _$fabricanteAtom.reportRead();
    return super.fabricante;
  }

  @override
  set fabricante(String value) {
    _$fabricanteAtom.reportWrite(value, super.fabricante, () {
      super.fabricante = value;
    });
  }

  final _$tipo_veiculoAtom =
      Atom(name: '_ConfiguracaoControllerBase.tipo_veiculo');

  @override
  String get tipo_veiculo {
    _$tipo_veiculoAtom.reportRead();
    return super.tipo_veiculo;
  }

  @override
  set tipo_veiculo(String value) {
    _$tipo_veiculoAtom.reportWrite(value, super.tipo_veiculo, () {
      super.tipo_veiculo = value;
    });
  }

  final _$combustivelAtom =
      Atom(name: '_ConfiguracaoControllerBase.combustivel');

  @override
  String get combustivel {
    _$combustivelAtom.reportRead();
    return super.combustivel;
  }

  @override
  set combustivel(String value) {
    _$combustivelAtom.reportWrite(value, super.combustivel, () {
      super.combustivel = value;
    });
  }

  final _$tamanho_motorAtom =
      Atom(name: '_ConfiguracaoControllerBase.tamanho_motor');

  @override
  String get tamanho_motor {
    _$tamanho_motorAtom.reportRead();
    return super.tamanho_motor;
  }

  @override
  set tamanho_motor(String value) {
    _$tamanho_motorAtom.reportWrite(value, super.tamanho_motor, () {
      super.tamanho_motor = value;
    });
  }

  final _$eficiencia_motorAtom =
      Atom(name: '_ConfiguracaoControllerBase.eficiencia_motor');

  @override
  String get eficiencia_motor {
    _$eficiencia_motorAtom.reportRead();
    return super.eficiencia_motor;
  }

  @override
  set eficiencia_motor(String value) {
    _$eficiencia_motorAtom.reportWrite(value, super.eficiencia_motor, () {
      super.eficiencia_motor = value;
    });
  }

  final _$consumo_combustivelAtom =
      Atom(name: '_ConfiguracaoControllerBase.consumo_combustivel');

  @override
  String get consumo_combustivel {
    _$consumo_combustivelAtom.reportRead();
    return super.consumo_combustivel;
  }

  @override
  set consumo_combustivel(String value) {
    _$consumo_combustivelAtom.reportWrite(value, super.consumo_combustivel, () {
      super.consumo_combustivel = value;
    });
  }

  final _$capacidade_tanqueAtom =
      Atom(name: '_ConfiguracaoControllerBase.capacidade_tanque');

  @override
  String get capacidade_tanque {
    _$capacidade_tanqueAtom.reportRead();
    return super.capacidade_tanque;
  }

  @override
  set capacidade_tanque(String value) {
    _$capacidade_tanqueAtom.reportWrite(value, super.capacidade_tanque, () {
      super.capacidade_tanque = value;
    });
  }

  final _$custo_unidadeAtom =
      Atom(name: '_ConfiguracaoControllerBase.custo_unidade');

  @override
  String get custo_unidade {
    _$custo_unidadeAtom.reportRead();
    return super.custo_unidade;
  }

  @override
  set custo_unidade(String value) {
    _$custo_unidadeAtom.reportWrite(value, super.custo_unidade, () {
      super.custo_unidade = value;
    });
  }

  final _$fator_escalaAtom =
      Atom(name: '_ConfiguracaoControllerBase.fator_escala');

  @override
  String get fator_escala {
    _$fator_escalaAtom.reportRead();
    return super.fator_escala;
  }

  @override
  set fator_escala(String value) {
    _$fator_escalaAtom.reportWrite(value, super.fator_escala, () {
      super.fator_escala = value;
    });
  }

  final _$getConfigAsyncAction =
      AsyncAction('_ConfiguracaoControllerBase.getConfig');

  @override
  Future getConfig() {
    return _$getConfigAsyncAction.run(() => super.getConfig());
  }

  final _$saveOrUpdateConfigAsyncAction =
      AsyncAction('_ConfiguracaoControllerBase.saveOrUpdateConfig');

  @override
  Future saveOrUpdateConfig() {
    return _$saveOrUpdateConfigAsyncAction
        .run(() => super.saveOrUpdateConfig());
  }

  @override
  String toString() {
    return '''
id: ${id},
ano: ${ano},
fabricante: ${fabricante},
tipo_veiculo: ${tipo_veiculo},
combustivel: ${combustivel},
tamanho_motor: ${tamanho_motor},
eficiencia_motor: ${eficiencia_motor},
consumo_combustivel: ${consumo_combustivel},
capacidade_tanque: ${capacidade_tanque},
custo_unidade: ${custo_unidade},
fator_escala: ${fator_escala}
    ''';
  }
}
