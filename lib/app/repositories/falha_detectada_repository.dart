import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/models/falha_detectada_model.dart';
import 'package:tcc_mobile/app/repositories/falha_detectada_interface.dart';

class FalhaDetectadaRepository implements IFalhaDectadaRepository{

  final ObservableList<FalhaDetectadaModel> falhas =  ObservableList<FalhaDetectadaModel>();

  @override
  ObservableList<FalhaDetectadaModel> getFalhas() {

    falhas.add(new FalhaDetectadaModel("P0351"));
    falhas.add(new FalhaDetectadaModel("P0361"));
    falhas.add(new FalhaDetectadaModel("P0362"));

    return falhas;
  }

  @override
  bool limarCodigos() {
    // TODO: implement limarCodigos with obd2
    falhas.clear();
    return true;
  }

}