import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/models/configuracao_model.dart';

import '../models/falha_detectada_model.dart';

abstract class IConfiguracaoRepository {
  Future<int> insert(ConfiguracaoModel config);
  Future<int> update(ConfiguracaoModel config);
  Future<int> deleteById(id);
  Future<ConfiguracaoModel> findById(int id);
  Future<int> getCount();
}