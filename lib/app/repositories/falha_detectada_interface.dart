import 'package:mobx/mobx.dart';

import '../models/falha_detectada_model.dart';

abstract class IFalhaDectadaRepository {
  ObservableList<FalhaDetectadaModel> getFalhas();
  bool limarCodigos();
}