import 'package:sqflite/sqflite.dart';
import 'package:tcc_mobile/app/models/configuracao_model.dart';
import 'package:tcc_mobile/app/repositories/configuracao_interface.dart';
import 'package:tcc_mobile/app/utils/database_provider.dart';

class ConfiguracaoRepository implements IConfiguracaoRepository{

  final ConfiguracaoModel ModelDb = new ConfiguracaoModel();

  Future<Database> _database() async {
    return await DatabaseProvider.instance.database;
  }

  @override
  Future<int> insert(ConfiguracaoModel config) async {
    final Database db = await _database();
    int id = await db.insert(ModelDb.tableConfig, config.toMap());
    return id;
  }

  @override
  Future<ConfiguracaoModel> findById(int id) async {
    final Database db = await _database();
    List<Map> maps = await db.query(ModelDb.tableConfig,
        columns: [ModelDb.columnId, ModelDb.columnAno, ModelDb.columnFabricante, ModelDb.columnTipoVeiculo,
          ModelDb.columnCombustivel, ModelDb.columnTamanhoMotor, ModelDb.columnEficienciaMotor, ModelDb.columnConsumoCombustivel,
          ModelDb.columnCapacidadeTanque, ModelDb.columnCustoUnidade, ModelDb.columnFatorEscala],
        where: '${ModelDb.columnId} = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return ConfiguracaoModel.fromMap(maps.first);
    }
    return null;
  }

  @override
  Future<int> update(ConfiguracaoModel config) async {
    final Database db = await _database();
    int rowsUpdated = await db.update(ModelDb.tableConfig, config.toMap(),
        where: "${ModelDb.columnId} = ?", whereArgs: [config.id]);
    return rowsUpdated;
  }

  @override
  Future<int> deleteById(id) async {
    final Database db = await _database();
    int rowsDeleted = await db.delete(ModelDb.tableConfig, where: "id = ?", whereArgs: [id]);
    return rowsDeleted;
  }

  Future<int> getCount() async {
    final Database db = await _database();
    List<Map> maps = await db.query(ModelDb.tableConfig,
        columns: [ModelDb.columnId]);
    return maps.length;
  }


}