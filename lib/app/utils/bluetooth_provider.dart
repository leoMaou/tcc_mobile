import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:mobx/mobx.dart';

class BluetoothProvider {
  // Make this a singleton class.
  BluetoothProvider._privateConstructor();

  static final BluetoothProvider instance = BluetoothProvider._privateConstructor();

  // Only allow a single open connection to the bluetooth.
  static FlutterBluetoothSerial _flutterBlueS;

  BluetoothConnection connection;

  BluetoothDiscoveryResult _ResultDeviceConnected;

  StreamSubscription<BluetoothDiscoveryResult> _streamSubscription;

  Future<FlutterBluetoothSerial> get getFlutterBlueS async {
    if (_flutterBlueS != null) return _flutterBlueS;
    _flutterBlueS = FlutterBluetoothSerial.instance;
    return _flutterBlueS;
  }


  sendCommand(String data) async {
    if(connection != null) {
      connection.output.add(utf8.encode(data + "\r\n"));
      await connection.output.allSent;
    }
  }

  StreamSubscription<BluetoothDiscoveryResult> scanDispositivos(ObservableList<BluetoothDiscoveryResult> results) {
    BluetoothDiscoveryResult discoveryConectado;

    if (_ResultDeviceConnected != null) {
      discoveryConectado = _ResultDeviceConnected;
    }

    results.clear();
    if (discoveryConectado != null) {
      results.add(discoveryConectado);
    }

    _streamSubscription = FlutterBluetoothSerial.instance.startDiscovery().listen((r) {
      if(discoveryConectado != null) {
        if(discoveryConectado.device.address != r.device.address) {
          results.add(r);
        }
      }else {
        results.add(r);
      }
    });

    return _streamSubscription;
  }

  stopScanListener() async {
    await this._streamSubscription.cancel();
    _streamSubscription = null;
  }

  Future<BluetoothDevice> conectarDispositivo(BluetoothDiscoveryResult resultDevice, callback) async {
    try {
      this.connection = await BluetoothConnection.toAddress(resultDevice.device.address);
      print("Conectado no dispostivo");
      _ResultDeviceConnected = resultDevice;
    } catch (e) {
      print("Não foi possivel se conectar, algo deu errado");
      connection = null;
      return null;
    }

    bool isDisconnecting = false;

    connection.input.listen((data) {
      _onDataReceived(data,callback);
    }).onDone(() {
      if (isDisconnecting) {
        print('Disconnecting locally!');
      } else {
        print('Disconnected remotely!');
      }
    });

    return _ResultDeviceConnected.device;
  }

  desconectarDispositivo() {
    if(this.connection != null) {
      try {
        this.connection.dispose();
        print("desconectado do dispostivo");
        this._ResultDeviceConnected = null;
      } catch (e) {
        print("Não foi possivel se desconectar, algo deu errado");
      }
    }
    return _ResultDeviceConnected;
  }

  String dataComplelto = "";

  void _onDataReceived(Uint8List data, callback) {
    // Allocate buffer for parsed data
    int backspacesCounter = 0;
    data.forEach((byte) {
      if (byte == 8 || byte == 127) {
        backspacesCounter++;
      }
    });
    Uint8List buffer = Uint8List(data.length - backspacesCounter);
    int bufferIndex = buffer.length;

    // Apply backspace control character
    backspacesCounter = 0;
    for (int i = data.length - 1; i >= 0; i--) {
      if (data[i] == 8 || data[i] == 127) {
        backspacesCounter++;
      } else {
        if (backspacesCounter > 0) {
          backspacesCounter--;
        } else {
          buffer[--bufferIndex] = data[i];
        }
      }
    }

    // Create message if there is new line character
    String dataString = String.fromCharCodes(buffer);
    dataComplelto += dataString;
    if(dataString.contains(">")) {
      callback(dataComplelto);
      dataComplelto = "";
    }

    RegExp regExp = RegExp(r'(atrv|atsp0|atdp|atz|atd)');
    if(regExp.hasMatch(dataComplelto)) {
//      callback(dataComplelto);
      dataComplelto = "";
    }

  }

}
