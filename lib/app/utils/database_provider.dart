import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:tcc_mobile/app/models/configuracao_model.dart';

class DatabaseProvider {

  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseProvider._privateConstructor();
  static final DatabaseProvider instance = DatabaseProvider._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  final ConfiguracaoModel configuracaoModel = new ConfiguracaoModel();

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE ${configuracaoModel.tableConfig} (
                ${configuracaoModel.columnId} INTEGER PRIMARY KEY,
                ${configuracaoModel.columnAno} INTEGER,
                ${configuracaoModel.columnFabricante} TEXT,
                ${configuracaoModel.columnTipoVeiculo} TEXT,
                ${configuracaoModel.columnCombustivel} TEXT,
                ${configuracaoModel.columnTamanhoMotor} TEXT,
                ${configuracaoModel.columnEficienciaMotor} TEXT,
                ${configuracaoModel.columnConsumoCombustivel} REAL,
                ${configuracaoModel.columnCapacidadeTanque} REAL,
                ${configuracaoModel.columnCustoUnidade} REAL,
                ${configuracaoModel.columnFatorEscala} REAL
              )
              ''');
  }

}