import 'package:flutter/material.dart';

Widget Side_menu(context) {
  return  ListView(
    children: <Widget>[
      new UserAccountsDrawerHeader(
        accountName: new Text("Pessoa"),
        accountEmail: new Text("pessoa@email.com"),
        currentAccountPicture: new CircleAvatar(
          backgroundImage:
          new NetworkImage("https://i.pravatar.cc/300"),
        ),
      ),
      new ListTile(
        title: new Text('Monitoramento'),
        leading: Icon(Icons.blur_circular),
        onTap: () {
          Navigator.popAndPushNamed(context, '/monitoramento/');
        },
      ),
      new ListTile(
        title: new Text('Configurações'),
        leading: Icon(Icons.build),
        onTap: () {
          Navigator.pushNamed(context, '/configuracao/');
        },
      ),
      new ListTile(
        title: new Text('Eficiência'),
        leading: Icon(Icons.assessment),
        onTap: () {
          Navigator.popAndPushNamed(context, '/eficiencia/');
        },
      ),
      new ListTile(
        title: new Text('Diário de bordo'),
        leading: Icon(Icons.assignment),
        onTap: () {
          Navigator.popAndPushNamed(context, '/diario_de_bordo/');
        },
      ),
      new ListTile(
        title: new Text('Pesquisar códigos'),
        leading: Icon(Icons.search),
        onTap: () {
          Navigator.popAndPushNamed(context, '/pesquisar/');
        },
      ),
      new ListTile(
        title: new Text('Rastreamento por GPS'),
        leading: Icon(Icons.gps_fixed),
        onTap: () {
          Navigator.popAndPushNamed(context, '/rastreamento/');
        },
      ),
      new ListTile(
        leading: Icon(Icons.arrow_back),
        title: new Text('Sair'),
        onTap: () {
          Navigator.popAndPushNamed(context, '/');
        },
      ),
    ],
  );
}