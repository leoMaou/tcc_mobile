import 'package:flutter/material.dart';
import 'package:tcc_mobile/app/app_module.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/utils/database_provider.dart';

void main() async {
  WidgetsFlutterBinding
      .ensureInitialized(); //permite carregar dados dados assincronos no main()
  await DatabaseProvider.instance.database;
  runApp(ModularApp(module: AppModule()));
}
